import org.testng.annotations.Test;
import pages.ExamplePage;

public class ExampleTest extends BaseTest {

    @Test
    public void clickWithoutForce() {
        new ExamplePage(driver)
                .pressExampleButtonWithoutForce();
    }

    @Test
    public void clickWithForce() {
        new ExamplePage(driver)
                .pressExampleButtonWithForce();
    }

}
