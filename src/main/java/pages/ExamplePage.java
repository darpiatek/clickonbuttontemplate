package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExamplePage extends BasePage {
    @FindBy(id = "didomi-notice-agree-button") //replace locator with yours
    private WebElement exampleButton;

    public ExamplePage(WebDriver driver) {
        super(driver);
    }

    public ExamplePage pressExampleButtonWithoutForce() {
        exampleButton.click();
        return this;
    }

    public ExamplePage pressExampleButtonWithForce() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", exampleButton);
        return this;
    }

}
